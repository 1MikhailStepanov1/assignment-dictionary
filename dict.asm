%include "lib.inc"
global find_word

;IN
;	rdi - null-terminated string pointer
;	rsi - dictionary start index
;
;Program searches through dictionary for value, which was inputted before, 
;andreturns address of entry with given string
;
;OUT
;	address - word in dictionary
;	0 - word is not in dictionary

find_word:
	test rsi, rsi
	je .end
	
	.loop:
		push rdi
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi
		
		test rax, rax
		jnz .match
		mov rsi, [rsi]
		cmp rsi, 0
		je .end
		jmp .loop
		
	.match:
		push rsi
		call string_length
		pop rsi
		add rsi, 9
		add rax, rsi
		ret

	.end:
		xor rax, rax
		ret
