extern find_word

global _start

section .rodata

%include "words.inc"
not_found_message: db "There is no matches in dicrionary with this key", 0
key_fail_message: db "Key is not valid (length of string is bigger than buffer or empty)", 0

section .bss
data_buffer: resb 256

section .text
%include "colon.inc"
%include "lib.inc"
_start:

;Key's validation

	mov rsi, 256
	mov rdi, data_buffer
	call read_input
	cmp rax, 0
	je .key_fail
	
;Trying to detect word from buffer
	mov rsi, dict_link
	mov rdi, data_buffer
	call find_word
	cmp rax, 0
	je .not_found

;Writing founded word

	mov rdi, rax
	call print_newline
	jmp .output
	
.key_fail:
	call print_newline
	mov rdi, key_fail_message
	jmp .output_error
	
.not_found:
	call print_newline
	mov rdi, not_found_message
	jmp .output_error

.output:
	call print_string
	call exit
	
.output_error:
	call print_error
	call exit
